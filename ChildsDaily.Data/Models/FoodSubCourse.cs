﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChildsDaily.Data.Models
{
    public class FoodSubCourse : BaseEntity
    {
        #region Table specific fields
        public int FoodSubCourseId { get; set; }       

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string Food { get; set; }   

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string FoodInfo { get; set; }

        [Range(1, 1000)]
        public string Weight { get; set; } 


        [Column(TypeName = "VARCHAR(70)")]
        public string Remarks { get; set; } 
       
        public char RecStatus { get; set; } = 'A';

        #endregion

        #region Child tables
        public List<Breakfast> Breakfasts { get; set; }
        public List<Lunch> Lunches { get; set; }
        public List<Brunch> Brunches { get; set; }

        #endregion

        #region Parent tables
        public int BranchId { get; set; }    
        public Branch Branch { get; set; } 

        #endregion
    }
}
