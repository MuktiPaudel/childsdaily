﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChildsDaily.Data.Models
{
    public class Child : BaseEntity
    {
        public int ChildId { get; set; } 

        [Required]
        public Guid ChildGuid { get; set; }  

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string FirstName { get; set; }

        [Column(TypeName = "VARCHAR(70)")]
        public string LastName { get; set; }

        [Range(1, 6)]
        public int Age { get; set; } 

        public ChildsGenders ChildsGender { get; set; }    
   
        [Column(TypeName = "VARCHAR(70)")]
        public string Remarks { get; set; }

        public char RecStatus { get; set; } = 'A';

        #region Parent tables
        public int ParentId { get; set; } 
        public Parent Parent { get; set; }
        public int GroupId { get; set; } 
        public Group Group { get; set; }
        public int NurseId { get; set; } 
        public Nurse Nurse { get; set; } 
        #endregion

        #region Child tables
        public List<Breakfast> Breakfasts { get; set; } 
        public List<Lunch> Lunches { get; set; } 
        public List<Brunch> Brunches { get; set; } 
        public List<ActivityAndException> ActivityAndExceptions { get; set; }   
        
        #endregion

        public enum ChildsGenders 
        {
            Male,
            Female,
            Other 
        }


    }
}
