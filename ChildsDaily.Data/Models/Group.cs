﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChildsDaily.Data.Models
{
    public class Group : BaseEntity
    {
        #region Table specific fields
        public int GroupId { get; set; } 

        [Required]
        public Guid GroupGuid { get; set; }   

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string Name { get; set; } 

        [Column(TypeName = "VARCHAR(70)")]
        public string ContactPerson { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string MobileNumber { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Column(TypeName = "VARCHAR(70)")]
        public string Remarks { get; set; }

        public char RecStatus { get; set; } = 'A';

        #endregion

        #region Child tables

        public List<Nurse> Nurses { get; set; } 
        public List<Child> Childern { get; set; }
        public List<Breakfast> Breakfasts { get; set; }
        public List<Lunch> Lunches { get; set; }
        public List<Brunch> Brunches { get; set; }
        public List<ActivityAndException> ActivityAndExceptions { get; set; }

        #endregion

        #region Parent tables
        public int BranchId { get; set; } 
        public Branch Branch { get; set; }
        public int ParentId { get; set; } 
        public Parent Parent { get; set; }  

        #endregion
    }
}
