﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChildsDaily.Data.Models
{
    public class Lunch : BaseEntity
    {
        #region Table specific fields
        public int LunchId { get; set; } 

        [Column(TypeName = "VARCHAR(70)")]
        public string Remarks { get; set; }
        public ConsumedFoodAmounts Amounts { get; set; } 
        public char RecStatus { get; set; } = 'A';

        #endregion

        #region Child tables

        #endregion
        #region Parent tables
        public int GroupId { get; set; }
        public Group Group { get; set; }
        public int NurseId { get; set; }
        public Nurse Nurse { get; set; }
        public int ChildId { get; set; }
        public Child Child { get; set; }
        public int FoodMainCourseId { get; set; }
        public FoodMainCourse FoodMainCourse { get; set; }
        public int FoodSubCourseId { get; set; }
        public FoodSubCourse FoodSubCourse { get; set; }
        public int FoodDrinkId { get; set; }
        public FoodDrink FoodDrink { get; set; }
        public int FoodSoupId { get; set; } 
        public FoodSoup FoodSoup { get; set; }
        #endregion
        public enum ConsumedFoodAmounts
        {
            None,
            Half,
            Finised
        }
    }
}
