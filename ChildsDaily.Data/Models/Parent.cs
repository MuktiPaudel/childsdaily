﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChildsDaily.Data.Models
{
    public class Parent : BaseEntity
    {
        public int ParentId { get; set; }

        [Required]
        public Guid ParentGuid { get; set; }    

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string FirstName { get; set; } 

        [Column(TypeName = "VARCHAR(70)")]
        public string LastName { get; set; }

        public ParentChildRelationship  Relationship { get; set; }  

        [DataType(DataType.PhoneNumber)]
        public string MobileNumber { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Column(TypeName = "VARCHAR(70)")]
        public string Remarks { get; set; }

        public char RecStatus { get; set; } = 'A';

        #region Child tables
        public List<Child> Childern { get; set; }
        public List<Group> Groups { get; set; }   
        #endregion

        #region Parent tables
        public int BranchId { get; set; } 
        public Branch Branch { get; set; }

        #endregion
        public enum ParentChildRelationship 
        {
            Father,
            Mother
        }
    }
}
