﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChildsDaily.Data.Models
{
    public class IndoorActivity : BaseEntity
    {
        #region Table specific fields
        public int IndoorActivityId { get; set; }       

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string Activity { get; set; }  

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string ActivityInfo { get; set; } 

        [Column(TypeName = "VARCHAR(70)")]
        public string Remarks { get; set; } 
       
        public char RecStatus { get; set; } = 'A';

        #endregion

        #region Child tables

        public List<ActivityAndException> ActivityAndExceptions { get; set; }   

        #endregion

        #region Parent tables
        public int BranchId { get; set; }    
        public Branch Branch { get; set; } 

        #endregion
    }
}
