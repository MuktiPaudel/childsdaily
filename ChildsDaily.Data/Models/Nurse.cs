﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChildsDaily.Data.Models
{
    public class Nurse : BaseEntity
    {
        #region Table specific fields
        public int NurseId { get; set; } 

        [Required]
        public Guid NurseGuid { get; set; }  

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string FirstName { get; set; }

        [Column(TypeName = "VARCHAR(70)")]
        public string LastName { get; set; }

        public NursesGenders Gender { get; set; } 

        [DataType(DataType.PhoneNumber)]
        public string MobileNumber { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Column(TypeName = "VARCHAR(70)")]
        public string Remarks { get; set; }

        public char RecStatus { get; set; } = 'A';

        #endregion

        #region Child tables 
        public List<Child> Childern { get; set; } 
        public List<Breakfast> Breakfasts { get; set; }
        public List<Lunch> Lunches { get; set; }
        public List<Brunch> Brunches { get; set; }
        public List<ActivityAndException> ActivityAndExceptions { get; set; }
        #endregion

        #region Parent tables
        public int GroupId { get; set; }   
        public Group Group { get; set; } 
        #endregion
        public enum NursesGenders 
        {
            Male,
            Female,
            Other
        }
    }
}
