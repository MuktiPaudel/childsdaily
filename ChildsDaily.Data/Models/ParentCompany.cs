﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChildsDaily.Data.Models
{
    public class ParentCompany : BaseEntity
    {

        #region Table specific fields
        public int ParentCompanyId { get; set; } 

        [Required]
        public Guid CompanyGuid { get; set; } 

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string Company { get; set; }

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string Country { get; set; }

        [Column(TypeName = "VARCHAR(70)")]
        public string City { get; set; }

        [Column(TypeName = "VARCHAR(70)")]
        public string Street { get; set; }

        [DataType(DataType.PostalCode)]
        public string PostalCode { get; set; }

        [Column(TypeName = "VARCHAR(70)")]
        public string ContactPerson { get; set; } 

        [DataType(DataType.PhoneNumber)]
        public string MobileNumber { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public int NumberOfBranches { get; set; } 

        [Column(TypeName = "VARCHAR(70)")]
        public string Remarks { get; set; }
        public char RecStatus { get; set; } = 'A';

        #endregion

        #region Child tables
        public List<Branch> Branches { get; set; }   
        #endregion
    }
}
