﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChildsDaily.Data.Models
{
    public class ActivityAndException : BaseEntity
    {
        #region Table specific fields
        public int ActivityAndExceptionId { get; set; } 

        [Range(0, 24)]
        public string IncidentsTime { get; set; }

        [Column(TypeName = "VARCHAR(70)")]
        public string Remarks { get; set; }       
        public char RecStatus { get; set; } = 'A';

        #endregion

        #region Child tables 

        #endregion
        #region Parent tables
        public int GroupId { get; set; } 
        public Group Group { get; set; }
        public int NurseId { get; set; } 
        public Nurse Nurse { get; set; }
        public int ChildId { get; set; } 
        public Child Child { get; set; }
        public int IndoorActivityId { get; set; }   
        public IndoorActivity IndoorActivity { get; set; } 
        public int OutdoorActivityId { get; set; } 
        public OutdoorActivity OutdoorActivity { get; set; } 
        public int ExceptionalConditionId { get; set; }  
        public ExceptionalCondition ExceptionalCondition { get; set; }  

        #endregion
 
    }
}
