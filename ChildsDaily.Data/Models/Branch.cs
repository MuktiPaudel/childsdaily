﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChildsDaily.Data.Models
{
    public class Branch : BaseEntity
    {

        #region Table specific fields
        public int BranchId { get; set; } 

        [Required]
        public Guid BranchGuid { get; set; }  

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string Country { get; set; } 

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string City { get; set; } 

        [Column(TypeName = "VARCHAR(70)")]
        public string Street { get; set; } 

        [DataType(DataType.PostalCode)]
        public string PostalCode { get; set; }

        [Column(TypeName = "VARCHAR(70)")]
        public string ContactPerson { get; set; } 

        [DataType(DataType.PhoneNumber)]
        public string MobileNumber { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Column(TypeName = "VARCHAR(70)")]
        public string Remarks { get; set; }
        public char RecStatus { get; set; } = 'A';
        #endregion

        #region Parent tables
        public int ParentCompanyId { get; set; }  
        public ParentCompany ParentCompany { get; set; }  
        #endregion

        #region Child tables
        public List<Group> Groups { get; set; } 
        public List<Parent> Parents { get; set; } 
        public List<FoodMainCourse> FoodMainCourses { get; set; }
        public List<FoodSubCourse> FoodSubCourses { get; set; }
        public List<FoodDrink> FoodDrinks { get; set; }
        public List<FoodSoup> FoodSoups { get; set; }  

        public List<IndoorActivity> IndoorActivities { get; set; }
        public List<OutdoorActivity> OutdoorActivities { get; set; }
        public List<ExceptionalCondition> ExceptionalConditions { get; set; }
        #endregion
    }
}
