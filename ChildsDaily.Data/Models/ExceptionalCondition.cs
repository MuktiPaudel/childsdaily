﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChildsDaily.Data.Models
{
    public class ExceptionalCondition : BaseEntity
    {
        #region Table specific fields
        public int ExceptionalConditionId { get; set; }       

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string Condition { get; set; }    

        [Required]
        [Column(TypeName = "VARCHAR(70)")]
        public string ConditionInfo { get; set; } 

        [Column(TypeName = "VARCHAR(70)")]
        public string Remarks { get; set; } 
       
        public char RecStatus { get; set; } = 'A';

        #endregion

        #region Child tables
        public List<ActivityAndException> ActivityAndExceptions { get; set; } 

        #endregion

        #region Parent tables
        public int BranchId { get; set; }   
        public Branch Branch { get; set; } 

        #endregion
    }
}
