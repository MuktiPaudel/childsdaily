﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ChildsDaily.Data.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ParentCompanies",
                columns: table => new
                {
                    ParentCompanyId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CompanyId = table.Column<Guid>(nullable: false),
                    Company = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    Country = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    City = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    Street = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    MobileNumber = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: false),
                    NumberOfBranches = table.Column<int>(nullable: false),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParentCompanies", x => x.ParentCompanyId);
                });

            migrationBuilder.CreateTable(
                name: "Branches",
                columns: table => new
                {
                    BranchId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    BranchGuid = table.Column<Guid>(nullable: false),
                    Country = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    City = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    Street = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    MobileNumber = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: false),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false),
                    ParentCompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Branches", x => x.BranchId);
                    table.ForeignKey(
                        name: "FK_Branches_ParentCompanies_ParentCompanyId",
                        column: x => x.ParentCompanyId,
                        principalTable: "ParentCompanies",
                        principalColumn: "ParentCompanyId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExceptionalConditions",
                columns: table => new
                {
                    ExceptionalConditionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    Condition = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    ConditionInfo = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false),
                    BranchId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExceptionalConditions", x => x.ExceptionalConditionId);
                    table.ForeignKey(
                        name: "FK_ExceptionalConditions_Branches_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branches",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FoodDrinks",
                columns: table => new
                {
                    FoodDrinkId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    Food = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    FoodInfo = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    Weight = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false),
                    BranchId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodDrinks", x => x.FoodDrinkId);
                    table.ForeignKey(
                        name: "FK_FoodDrinks_Branches_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branches",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FoodMainCourses",
                columns: table => new
                {
                    FoodMainCourseId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    Food = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    FoodInfo = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    Weight = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false),
                    BranchId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodMainCourses", x => x.FoodMainCourseId);
                    table.ForeignKey(
                        name: "FK_FoodMainCourses_Branches_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branches",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FoodSoup",
                columns: table => new
                {
                    FoodSoupId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    Food = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    FoodInfo = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    Weight = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false),
                    BranchId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodSoup", x => x.FoodSoupId);
                    table.ForeignKey(
                        name: "FK_FoodSoup_Branches_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branches",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FoodSubCourses",
                columns: table => new
                {
                    FoodSubCourseId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    Food = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    FoodInfo = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    Weight = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false),
                    BranchId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodSubCourses", x => x.FoodSubCourseId);
                    table.ForeignKey(
                        name: "FK_FoodSubCourses_Branches_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branches",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "IndoorActivities",
                columns: table => new
                {
                    IndoorActivityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    Activity = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    ActivityInfo = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false),
                    BranchId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IndoorActivities", x => x.IndoorActivityId);
                    table.ForeignKey(
                        name: "FK_IndoorActivities_Branches_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branches",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OutdoorActivities",
                columns: table => new
                {
                    OutdoorActivityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    Activity = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    ActivityInfo = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false),
                    BranchId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutdoorActivities", x => x.OutdoorActivityId);
                    table.ForeignKey(
                        name: "FK_OutdoorActivities_Branches_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branches",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Parents",
                columns: table => new
                {
                    ParentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ParentGuid = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    LastName = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    Relationship = table.Column<int>(nullable: false),
                    MobileNumber = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: false),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false),
                    BranchId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parents", x => x.ParentId);
                    table.ForeignKey(
                        name: "FK_Parents_Branches_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branches",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    GroupId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    GroupGuid = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    ContactPerson = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    MobileNumber = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: false),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false),
                    BranchId = table.Column<int>(nullable: false),
                    ParentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.GroupId);
                    table.ForeignKey(
                        name: "FK_Groups_Branches_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branches",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Groups_Parents_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Parents",
                        principalColumn: "ParentId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Nurses",
                columns: table => new
                {
                    NurseId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    NurseGuid = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    LastName = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    MobileNumber = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: false),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false),
                    GroupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Nurses", x => x.NurseId);
                    table.ForeignKey(
                        name: "FK_Nurses_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "GroupId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Childern",
                columns: table => new
                {
                    ChildId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ChildGuid = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(type: "VARCHAR(70)", nullable: false),
                    LastName = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    Age = table.Column<int>(nullable: false),
                    ChildsGender = table.Column<int>(nullable: false),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false),
                    ParentId = table.Column<int>(nullable: false),
                    GroupId = table.Column<int>(nullable: false),
                    NurseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Childern", x => x.ChildId);
                    table.ForeignKey(
                        name: "FK_Childern_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "GroupId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Childern_Nurses_NurseId",
                        column: x => x.NurseId,
                        principalTable: "Nurses",
                        principalColumn: "NurseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Childern_Parents_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Parents",
                        principalColumn: "ParentId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ActivityAndExceptions",
                columns: table => new
                {
                    ActivityAndExceptionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    IncidentsTime = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    RecStatus = table.Column<string>(nullable: false),
                    GroupId = table.Column<int>(nullable: false),
                    NurseId = table.Column<int>(nullable: false),
                    ChildId = table.Column<int>(nullable: false),
                    IndoorActivityId = table.Column<int>(nullable: false),
                    OutdoorActivityId = table.Column<int>(nullable: false),
                    ExceptionalConditionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityAndExceptions", x => x.ActivityAndExceptionId);
                    table.ForeignKey(
                        name: "FK_ActivityAndExceptions_Childern_ChildId",
                        column: x => x.ChildId,
                        principalTable: "Childern",
                        principalColumn: "ChildId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ActivityAndExceptions_ExceptionalConditions_ExceptionalConditionId",
                        column: x => x.ExceptionalConditionId,
                        principalTable: "ExceptionalConditions",
                        principalColumn: "ExceptionalConditionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ActivityAndExceptions_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "GroupId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ActivityAndExceptions_IndoorActivities_IndoorActivityId",
                        column: x => x.IndoorActivityId,
                        principalTable: "IndoorActivities",
                        principalColumn: "IndoorActivityId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ActivityAndExceptions_Nurses_NurseId",
                        column: x => x.NurseId,
                        principalTable: "Nurses",
                        principalColumn: "NurseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ActivityAndExceptions_OutdoorActivities_OutdoorActivityId",
                        column: x => x.OutdoorActivityId,
                        principalTable: "OutdoorActivities",
                        principalColumn: "OutdoorActivityId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Breakfasts",
                columns: table => new
                {
                    BreakfastId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    Amounts = table.Column<int>(nullable: false),
                    RecStatus = table.Column<string>(nullable: false),
                    GroupId = table.Column<int>(nullable: false),
                    NurseId = table.Column<int>(nullable: false),
                    ChildId = table.Column<int>(nullable: false),
                    FoodMainCourseId = table.Column<int>(nullable: false),
                    FoodSubCourseId = table.Column<int>(nullable: false),
                    FoodDrinkId = table.Column<int>(nullable: false),
                    FoodSoupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Breakfasts", x => x.BreakfastId);
                    table.ForeignKey(
                        name: "FK_Breakfasts_Childern_ChildId",
                        column: x => x.ChildId,
                        principalTable: "Childern",
                        principalColumn: "ChildId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Breakfasts_FoodDrinks_FoodDrinkId",
                        column: x => x.FoodDrinkId,
                        principalTable: "FoodDrinks",
                        principalColumn: "FoodDrinkId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Breakfasts_FoodMainCourses_FoodMainCourseId",
                        column: x => x.FoodMainCourseId,
                        principalTable: "FoodMainCourses",
                        principalColumn: "FoodMainCourseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Breakfasts_FoodSoup_FoodSoupId",
                        column: x => x.FoodSoupId,
                        principalTable: "FoodSoup",
                        principalColumn: "FoodSoupId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Breakfasts_FoodSubCourses_FoodSubCourseId",
                        column: x => x.FoodSubCourseId,
                        principalTable: "FoodSubCourses",
                        principalColumn: "FoodSubCourseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Breakfasts_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "GroupId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Breakfasts_Nurses_NurseId",
                        column: x => x.NurseId,
                        principalTable: "Nurses",
                        principalColumn: "NurseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Brunches",
                columns: table => new
                {
                    BrunchId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    Amounts = table.Column<int>(nullable: false),
                    RecStatus = table.Column<string>(nullable: false),
                    GroupId = table.Column<int>(nullable: false),
                    NurseId = table.Column<int>(nullable: false),
                    ChildId = table.Column<int>(nullable: false),
                    FoodMainCourseId = table.Column<int>(nullable: false),
                    FoodSubCourseId = table.Column<int>(nullable: false),
                    FoodDrinkId = table.Column<int>(nullable: false),
                    FoodSoupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brunches", x => x.BrunchId);
                    table.ForeignKey(
                        name: "FK_Brunches_Childern_ChildId",
                        column: x => x.ChildId,
                        principalTable: "Childern",
                        principalColumn: "ChildId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Brunches_FoodDrinks_FoodDrinkId",
                        column: x => x.FoodDrinkId,
                        principalTable: "FoodDrinks",
                        principalColumn: "FoodDrinkId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Brunches_FoodMainCourses_FoodMainCourseId",
                        column: x => x.FoodMainCourseId,
                        principalTable: "FoodMainCourses",
                        principalColumn: "FoodMainCourseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Brunches_FoodSoup_FoodSoupId",
                        column: x => x.FoodSoupId,
                        principalTable: "FoodSoup",
                        principalColumn: "FoodSoupId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Brunches_FoodSubCourses_FoodSubCourseId",
                        column: x => x.FoodSubCourseId,
                        principalTable: "FoodSubCourses",
                        principalColumn: "FoodSubCourseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Brunches_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "GroupId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Brunches_Nurses_NurseId",
                        column: x => x.NurseId,
                        principalTable: "Nurses",
                        principalColumn: "NurseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Lunches",
                columns: table => new
                {
                    LunchId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(type: "VARCHAR(70)", nullable: true),
                    Amounts = table.Column<int>(nullable: false),
                    RecStatus = table.Column<string>(nullable: false),
                    GroupId = table.Column<int>(nullable: false),
                    NurseId = table.Column<int>(nullable: false),
                    ChildId = table.Column<int>(nullable: false),
                    FoodMainCourseId = table.Column<int>(nullable: false),
                    FoodSubCourseId = table.Column<int>(nullable: false),
                    FoodDrinkId = table.Column<int>(nullable: false),
                    FoodSoupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lunches", x => x.LunchId);
                    table.ForeignKey(
                        name: "FK_Lunches_Childern_ChildId",
                        column: x => x.ChildId,
                        principalTable: "Childern",
                        principalColumn: "ChildId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Lunches_FoodDrinks_FoodDrinkId",
                        column: x => x.FoodDrinkId,
                        principalTable: "FoodDrinks",
                        principalColumn: "FoodDrinkId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Lunches_FoodMainCourses_FoodMainCourseId",
                        column: x => x.FoodMainCourseId,
                        principalTable: "FoodMainCourses",
                        principalColumn: "FoodMainCourseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Lunches_FoodSoup_FoodSoupId",
                        column: x => x.FoodSoupId,
                        principalTable: "FoodSoup",
                        principalColumn: "FoodSoupId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Lunches_FoodSubCourses_FoodSubCourseId",
                        column: x => x.FoodSubCourseId,
                        principalTable: "FoodSubCourses",
                        principalColumn: "FoodSubCourseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Lunches_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "GroupId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Lunches_Nurses_NurseId",
                        column: x => x.NurseId,
                        principalTable: "Nurses",
                        principalColumn: "NurseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivityAndExceptions_ChildId",
                table: "ActivityAndExceptions",
                column: "ChildId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityAndExceptions_ExceptionalConditionId",
                table: "ActivityAndExceptions",
                column: "ExceptionalConditionId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityAndExceptions_GroupId",
                table: "ActivityAndExceptions",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityAndExceptions_IndoorActivityId",
                table: "ActivityAndExceptions",
                column: "IndoorActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityAndExceptions_NurseId",
                table: "ActivityAndExceptions",
                column: "NurseId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityAndExceptions_OutdoorActivityId",
                table: "ActivityAndExceptions",
                column: "OutdoorActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_Branches_ParentCompanyId",
                table: "Branches",
                column: "ParentCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Breakfasts_ChildId",
                table: "Breakfasts",
                column: "ChildId");

            migrationBuilder.CreateIndex(
                name: "IX_Breakfasts_FoodDrinkId",
                table: "Breakfasts",
                column: "FoodDrinkId");

            migrationBuilder.CreateIndex(
                name: "IX_Breakfasts_FoodMainCourseId",
                table: "Breakfasts",
                column: "FoodMainCourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Breakfasts_FoodSoupId",
                table: "Breakfasts",
                column: "FoodSoupId");

            migrationBuilder.CreateIndex(
                name: "IX_Breakfasts_FoodSubCourseId",
                table: "Breakfasts",
                column: "FoodSubCourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Breakfasts_GroupId",
                table: "Breakfasts",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Breakfasts_NurseId",
                table: "Breakfasts",
                column: "NurseId");

            migrationBuilder.CreateIndex(
                name: "IX_Brunches_ChildId",
                table: "Brunches",
                column: "ChildId");

            migrationBuilder.CreateIndex(
                name: "IX_Brunches_FoodDrinkId",
                table: "Brunches",
                column: "FoodDrinkId");

            migrationBuilder.CreateIndex(
                name: "IX_Brunches_FoodMainCourseId",
                table: "Brunches",
                column: "FoodMainCourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Brunches_FoodSoupId",
                table: "Brunches",
                column: "FoodSoupId");

            migrationBuilder.CreateIndex(
                name: "IX_Brunches_FoodSubCourseId",
                table: "Brunches",
                column: "FoodSubCourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Brunches_GroupId",
                table: "Brunches",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Brunches_NurseId",
                table: "Brunches",
                column: "NurseId");

            migrationBuilder.CreateIndex(
                name: "IX_Childern_GroupId",
                table: "Childern",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Childern_NurseId",
                table: "Childern",
                column: "NurseId");

            migrationBuilder.CreateIndex(
                name: "IX_Childern_ParentId",
                table: "Childern",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_ExceptionalConditions_BranchId",
                table: "ExceptionalConditions",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_FoodDrinks_BranchId",
                table: "FoodDrinks",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_FoodMainCourses_BranchId",
                table: "FoodMainCourses",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_FoodSoup_BranchId",
                table: "FoodSoup",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_FoodSubCourses_BranchId",
                table: "FoodSubCourses",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_Groups_BranchId",
                table: "Groups",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_Groups_ParentId",
                table: "Groups",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_IndoorActivities_BranchId",
                table: "IndoorActivities",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_Lunches_ChildId",
                table: "Lunches",
                column: "ChildId");

            migrationBuilder.CreateIndex(
                name: "IX_Lunches_FoodDrinkId",
                table: "Lunches",
                column: "FoodDrinkId");

            migrationBuilder.CreateIndex(
                name: "IX_Lunches_FoodMainCourseId",
                table: "Lunches",
                column: "FoodMainCourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Lunches_FoodSoupId",
                table: "Lunches",
                column: "FoodSoupId");

            migrationBuilder.CreateIndex(
                name: "IX_Lunches_FoodSubCourseId",
                table: "Lunches",
                column: "FoodSubCourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Lunches_GroupId",
                table: "Lunches",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Lunches_NurseId",
                table: "Lunches",
                column: "NurseId");

            migrationBuilder.CreateIndex(
                name: "IX_Nurses_GroupId",
                table: "Nurses",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_OutdoorActivities_BranchId",
                table: "OutdoorActivities",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_Parents_BranchId",
                table: "Parents",
                column: "BranchId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivityAndExceptions");

            migrationBuilder.DropTable(
                name: "Breakfasts");

            migrationBuilder.DropTable(
                name: "Brunches");

            migrationBuilder.DropTable(
                name: "Lunches");

            migrationBuilder.DropTable(
                name: "ExceptionalConditions");

            migrationBuilder.DropTable(
                name: "IndoorActivities");

            migrationBuilder.DropTable(
                name: "OutdoorActivities");

            migrationBuilder.DropTable(
                name: "Childern");

            migrationBuilder.DropTable(
                name: "FoodDrinks");

            migrationBuilder.DropTable(
                name: "FoodMainCourses");

            migrationBuilder.DropTable(
                name: "FoodSoup");

            migrationBuilder.DropTable(
                name: "FoodSubCourses");

            migrationBuilder.DropTable(
                name: "Nurses");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "Parents");

            migrationBuilder.DropTable(
                name: "Branches");

            migrationBuilder.DropTable(
                name: "ParentCompanies");
        }
    }
}
