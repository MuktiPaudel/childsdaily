﻿using ChildsDaily.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChildsDaily.Data
{
    public class ChildsDailyDbContext : DbContext
    {
        public ChildsDailyDbContext(DbContextOptions<ChildsDailyDbContext> options) : base(options) { }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }
        public DbSet<ParentCompany> ParentCompanies { get; set; }

        #region ParentCompany Child tables
        public DbSet<Branch> Branches { get; set; }
        #endregion
        #region Branch Child tables

        public DbSet<Group> Groups { get; set; }
        public DbSet<Parent> Parents { get; set; }
        public DbSet<FoodDrink> FoodDrinks { get; set; }
        public DbSet<FoodMainCourse> FoodMainCourses { get; set; }
        public DbSet<FoodSubCourse> FoodSubCourses { get; set; }
        public DbSet<FoodSoup> FoodSoup { get; set; }

        public DbSet<IndoorActivity> IndoorActivities { get; set; }
        public DbSet<OutdoorActivity> OutdoorActivities { get; set; }
        public DbSet<ExceptionalCondition> ExceptionalConditions { get; set; }

        #endregion

        #region Group child tables
        public DbSet<Nurse> Nurses { get; set; }
        public DbSet<Child> Childern { get; set; }
        #endregion

        #region Parents child tables
        //public DbSet<Child> Childerns { get; set; }  
        #endregion

        #region FoodDrink, OutdoorActivities, ExceptionalConditions child tables
        public DbSet<Breakfast> Breakfasts { get; set; }
        public DbSet<Lunch> Lunches { get; set; }
        public DbSet<Brunch> Brunches { get; set; }
        #endregion

        #region IndoorActivity, MainCourse, SubCourse, Soup child tables
        public DbSet<ActivityAndException> ActivityAndExceptions { get; set; }
        #endregion

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                var entity = entry.Entity;
                if (entry.State == EntityState.Deleted && entity is BaseEntity)
                {
                    entry.State = EntityState.Modified;
                    entity.GetType().GetProperty("RecStatus").SetValue(entity, 'D');

                }
            }
            var entries = ChangeTracker
               .Entries()
               .Where(e => e.Entity is BaseEntity && (
                       e.State == EntityState.Added
                       || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                ((BaseEntity)entityEntry.Entity).ModifiedDate = DateTime.Now;
                ((BaseEntity)entityEntry.Entity).ModifiedBy = "Mukti";

                if (entityEntry.State == EntityState.Added)
                {
                    ((BaseEntity)entityEntry.Entity).CreatedDate = DateTime.Now;
                    ((BaseEntity)entityEntry.Entity).CreatedBy = "Arjun";
                }
            }
            return base.SaveChanges();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Parent child relationsip of the tables

            #region First level tables

            modelBuilder.Entity<Branch>()
                .HasOne(p => p.ParentCompany)
                .WithMany(b => b.Branches)
                .HasForeignKey(p => p.ParentCompanyId)
                .OnDelete(DeleteBehavior.Restrict);

            #endregion

            #region Second level tables

            modelBuilder.Entity<Parent>()
               .HasOne(p => p.Branch)
               .WithMany(b => b.Parents)
               .HasForeignKey(p => p.BranchId)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<FoodMainCourse>()
               .HasOne(p => p.Branch)
               .WithMany(b => b.FoodMainCourses)
               .HasForeignKey(p => p.BranchId)
               .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<FoodSubCourse>()
               .HasOne(p => p.Branch)
               .WithMany(b => b.FoodSubCourses)
               .HasForeignKey(p => p.BranchId)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<FoodDrink>()
               .HasOne(p => p.Branch)
               .WithMany(b => b.FoodDrinks)
               .HasForeignKey(p => p.BranchId)
               .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<FoodSoup>()
               .HasOne(p => p.Branch)
               .WithMany(b => b.FoodSoups)
               .HasForeignKey(p => p.BranchId)
               .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<IndoorActivity>()
               .HasOne(p => p.Branch)
               .WithMany(b => b.IndoorActivities)
               .HasForeignKey(p => p.BranchId)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<OutdoorActivity>()
               .HasOne(p => p.Branch)
               .WithMany(b => b.OutdoorActivities)
               .HasForeignKey(p => p.BranchId)
               .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<ExceptionalCondition>()
               .HasOne(p => p.Branch)
               .WithMany(b => b.ExceptionalConditions)
               .HasForeignKey(p => p.BranchId)
               .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<Group>()
               .HasOne(p => p.Parent)
               .WithMany(b => b.Groups)
               .HasForeignKey(p => p.ParentId)
               .OnDelete(DeleteBehavior.Restrict);

            #endregion

            #region Third level tables

            modelBuilder.Entity<Nurse>()
               .HasOne(p => p.Group)
               .WithMany(b => b.Nurses)
               .HasForeignKey(p => p.GroupId)
               .OnDelete(DeleteBehavior.Restrict);

            #endregion

            #region Fourth level tables

            modelBuilder.Entity<Child>()
               .HasOne(p => p.Group)
               .WithMany(b => b.Childern)
               .HasForeignKey(p => p.GroupId)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Child>()
               .HasOne(p => p.Nurse)
               .WithMany(b => b.Childern)
               .HasForeignKey(p => p.NurseId)
               .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Child>()
               .HasOne(p => p.Parent)
               .WithMany(b => b.Childern)
               .HasForeignKey(p => p.ParentId)
               .OnDelete(DeleteBehavior.Restrict);

            #endregion

            #region Fifth level tables

            #region Breakfast parent tables setup

            modelBuilder.Entity<Breakfast>()
                .HasOne(p => p.Group)
                .WithMany(b => b.Breakfasts)
                .HasForeignKey(p => p.GroupId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Breakfast>()
                .HasOne(p => p.Child)
                .WithMany(b => b.Breakfasts)
                .HasForeignKey(p => p.ChildId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Breakfast>()
                .HasOne(p => p.Nurse)
                .WithMany(b => b.Breakfasts)
                .HasForeignKey(p => p.NurseId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Breakfast>()
                .HasOne(p => p.FoodMainCourse)
                .WithMany(b => b.Breakfasts)
                .HasForeignKey(p => p.FoodMainCourseId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Breakfast>()
                .HasOne(p => p.FoodSubCourse)
                .WithMany(b => b.Breakfasts)
                .HasForeignKey(p => p.FoodSubCourseId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Breakfast>()
                .HasOne(p => p.FoodDrink)
                .WithMany(b => b.Breakfasts)
                .HasForeignKey(p => p.FoodDrinkId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Breakfast>()
                .HasOne(p => p.FoodSoup)
                .WithMany(b => b.Breakfasts)
                .HasForeignKey(p => p.FoodSoupId)
                .OnDelete(DeleteBehavior.Restrict);

            #endregion

            #region Lunch parent tables setup

            modelBuilder.Entity<Lunch>()
                .HasOne(p => p.Group)
                .WithMany(b => b.Lunches)
                .HasForeignKey(p => p.GroupId)
                .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<Lunch>()
                .HasOne(p => p.Child)
                .WithMany(b => b.Lunches)
                .HasForeignKey(p => p.ChildId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Lunch>()
                .HasOne(p => p.Nurse)
                .WithMany(b => b.Lunches)
                .HasForeignKey(p => p.NurseId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Lunch>()
                .HasOne(p => p.FoodMainCourse)
                .WithMany(b => b.Lunches)
                .HasForeignKey(p => p.FoodMainCourseId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Lunch>()
                .HasOne(p => p.FoodSubCourse)
                .WithMany(b => b.Lunches)
                .HasForeignKey(p => p.FoodSubCourseId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Lunch>()
                .HasOne(p => p.FoodDrink)
                .WithMany(b => b.Lunches)
                .HasForeignKey(p => p.FoodDrinkId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Lunch>()
                .HasOne(p => p.FoodSoup)
                .WithMany(b => b.Lunches)
                .HasForeignKey(p => p.FoodSoupId)
                .OnDelete(DeleteBehavior.Restrict);

            #endregion

            #region Brunch parents tables setup

            modelBuilder.Entity<Brunch>()
                .HasOne(p => p.Group)
                .WithMany(b => b.Brunches)
                .HasForeignKey(p => p.GroupId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Brunch>()
                .HasOne(p => p.Child)
                .WithMany(b => b.Brunches)
                .HasForeignKey(p => p.ChildId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Brunch>()
                .HasOne(p => p.Nurse)
                .WithMany(b => b.Brunches)
                .HasForeignKey(p => p.NurseId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Brunch>()
                .HasOne(p => p.FoodMainCourse)
                .WithMany(b => b.Brunches)
                .HasForeignKey(p => p.FoodMainCourseId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Brunch>()
                .HasOne(p => p.FoodSubCourse)
                .WithMany(b => b.Brunches)
                .HasForeignKey(p => p.FoodSubCourseId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Brunch>()
                .HasOne(p => p.FoodDrink)
                .WithMany(b => b.Brunches)
                .HasForeignKey(p => p.FoodDrinkId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Brunch>()
                .HasOne(p => p.FoodSoup)
                .WithMany(b => b.Brunches)
                .HasForeignKey(p => p.FoodSoupId)
                .OnDelete(DeleteBehavior.Restrict);

            #endregion

            #region ActivitiesAndExceptions parent tables setup

            modelBuilder.Entity<ActivityAndException>()
                .HasOne(p => p.Group)
                .WithMany(b => b.ActivityAndExceptions)
                .HasForeignKey(p => p.GroupId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ActivityAndException>()
                .HasOne(p => p.Child)
                .WithMany(b => b.ActivityAndExceptions)
                .HasForeignKey(p => p.ChildId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ActivityAndException>()
                .HasOne(p => p.Nurse)
                .WithMany(b => b.ActivityAndExceptions)
                .HasForeignKey(p => p.NurseId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ActivityAndException>()
                .HasOne(p => p.IndoorActivity)
                .WithMany(b => b.ActivityAndExceptions)
                .HasForeignKey(p => p.IndoorActivityId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ActivityAndException>()
                .HasOne(p => p.OutdoorActivity)
                .WithMany(b => b.ActivityAndExceptions)
                .HasForeignKey(p => p.OutdoorActivityId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ActivityAndException>()
                .HasOne(p => p.ExceptionalCondition)
                .WithMany(b => b.ActivityAndExceptions)
                .HasForeignKey(p => p.ExceptionalConditionId)
                .OnDelete(DeleteBehavior.Restrict);

            #endregion

            #endregion

            #endregion

            #region Tables fields/columns 

            #region Parent Company fields

            modelBuilder.Entity<ParentCompany>().HasKey(s => s.ParentCompanyId);
            modelBuilder.Entity<ParentCompany>(entity =>
            {
                entity.Property(e => e.CompanyGuid)
                    .HasColumnName("CompanyId");

                entity.Property(e => e.Company)
                   .HasColumnName("Company");

                entity.Property(e => e.Country)
                    .HasColumnName("Country");

                entity.Property(e => e.City)
                    .HasColumnName("City");

                entity.Property(e => e.Street)
                    .HasColumnName("Street");

                entity.Property(e => e.PostalCode)
                    .HasColumnName("PostalCode");

                entity.Property(e => e.NumberOfBranches)
                    .HasColumnName("NumberOfBranches");

                entity.Property(e => e.Email)
                    .HasColumnName("Email");

                entity.Property(e => e.MobileNumber)
                    .HasColumnName("MobileNumber");

                entity.Property(e => e.ContactPerson)
                    .HasColumnName("ContactPerson");

                entity.Property(e => e.Remarks)
                    .HasColumnName("Remarks");

                entity.Property(e => e.RecStatus)
                    .HasColumnName("RecStatus");

            });

            #endregion

            #region Branches table fields

            modelBuilder.Entity<Branch>().HasKey(s => s.BranchId);
            modelBuilder.Entity<Branch>(entity =>
            {
                entity.Property(e => e.BranchGuid)
                    .HasColumnName("BranchGuid");

                entity.Property(e => e.Country)
                    .HasColumnName("Country");

                entity.Property(e => e.City)
                    .HasColumnName("City");

                entity.Property(e => e.Street)
                    .HasColumnName("Street");

                entity.Property(e => e.PostalCode)
                     .HasColumnName("PostalCode");

                entity.Property(e => e.Email)
                    .HasColumnName("Email");

                entity.Property(e => e.MobileNumber)
                    .HasColumnName("MobileNumber");

                entity.Property(e => e.ContactPerson)
                    .HasColumnName("ContactPerson");

                entity.Property(e => e.Remarks)
                    .HasColumnName("Remarks");

                entity.Property(e => e.RecStatus)
                    .HasColumnName("RecStatus");


            });
            #endregion

            #region Group table fields

            modelBuilder.Entity<Group>().HasKey(s => s.GroupId);
            modelBuilder.Entity<Group>(entity =>
            {
                entity.Property(e => e.GroupGuid)
                    .HasColumnName("GroupGuid");

                entity.Property(e => e.Name)
                    .HasColumnName("Name");

                entity.Property(e => e.Email)
                    .HasColumnName("Email");

                entity.Property(e => e.MobileNumber)
                    .HasColumnName("MobileNumber");

                entity.Property(e => e.ContactPerson)
                    .HasColumnName("ContactPerson");

                entity.Property(e => e.Remarks)
                     .HasColumnName("Remarks");

                entity.Property(e => e.RecStatus)
                    .HasColumnName("RecStatus");


            });
            #endregion

            #region Parent table fields

            modelBuilder.Entity<Parent>().HasKey(s => s.ParentId);
            modelBuilder.Entity<Parent>(entity =>
            {
                entity.Property(e => e.ParentGuid)
                    .HasColumnName("ParentGuid");

                entity.Property(e => e.FirstName)
                    .HasColumnName("FirstName");

                entity.Property(e => e.LastName)
                    .HasColumnName("LastName");

                entity.Property(e => e.Email)
                   .HasColumnName("Email");

                entity.Property(e => e.MobileNumber)
                    .HasColumnName("MobileNumber");

                entity.Property(e => e.Remarks)
                    .HasColumnName("Remarks");

                entity.Property(e => e.RecStatus)
                    .HasColumnName("RecStatus");
            });

            #endregion

            #region FoodDrink table fields

            modelBuilder.Entity<FoodDrink>().HasKey(s => s.FoodDrinkId);
            modelBuilder.Entity<FoodDrink>(entity =>
            {
                entity.Property(e => e.Food)
                    .HasColumnName("Food");

                entity.Property(e => e.FoodInfo)
                    .HasColumnName("FoodInfo");

                entity.Property(e => e.Weight)
                   .HasColumnName("Weight");

                entity.Property(e => e.Remarks)
                   .HasColumnName("Remarks");

                entity.Property(e => e.RecStatus)
                   .HasColumnName("RecStatus");

            });

            #endregion

            #region FoodMainCourse table fields

            modelBuilder.Entity<FoodMainCourse>().HasKey(s => s.FoodMainCourseId);
            modelBuilder.Entity<FoodMainCourse>(entity =>
            {
                entity.Property(e => e.Food)
                   .HasColumnName("Food");

                entity.Property(e => e.FoodInfo)
                    .HasColumnName("FoodInfo");

                 entity.Property(e => e.Weight)
                    .HasColumnName("Weight");

                 entity.Property(e => e.Remarks)
                    .HasColumnName("Remarks");

                 entity.Property(e => e.RecStatus)
                    .HasColumnName("RecStatus");

             });

            #endregion

            #region FoodSubCourse table fields

            modelBuilder.Entity<FoodSubCourse>().HasKey(s => s.FoodSubCourseId);
            modelBuilder.Entity<FoodSubCourse>(entity =>
            {
                entity.Property(e => e.Food)
                    .HasColumnName("Food");

                entity.Property(e => e.FoodInfo)
                     .HasColumnName("FoodInfo");

                entity.Property(e => e.Weight)
                 .HasColumnName("Weight");

                entity.Property(e => e.Remarks)
                   .HasColumnName("Remarks");

                entity.Property(e => e.RecStatus)
                     .HasColumnName("RecStatus");

            });

            #endregion

            #region FoodSoup table fields

            modelBuilder.Entity<FoodSoup>().HasKey(s => s.FoodSoupId);
            modelBuilder.Entity<FoodSoup>(entity =>
            {
                entity.Property(e => e.Food)
                    .HasColumnName("Food");

                entity.Property(e => e.FoodInfo)
                     .HasColumnName("FoodInfo");

                entity.Property(e => e.Weight)
                 .HasColumnName("Weight");

                entity.Property(e => e.Remarks)
                   .HasColumnName("Remarks");

                entity.Property(e => e.RecStatus)
                     .HasColumnName("RecStatus");

            });

            #endregion

            #region IndoorActivity table fields

            modelBuilder.Entity<IndoorActivity>().HasKey(s => s.IndoorActivityId);
            modelBuilder.Entity<IndoorActivity>(entity =>
            {
                entity.Property(e => e.Activity)
                    .HasColumnName("Activity");

                entity.Property(e => e.ActivityInfo)
                     .HasColumnName("ActivityInfo");

                entity.Property(e => e.Remarks)
                   .HasColumnName("Remarks");

                entity.Property(e => e.RecStatus)
                     .HasColumnName("RecStatus");

            });

            #endregion

            #region OutdoorActivity table fields

            modelBuilder.Entity<OutdoorActivity>().HasKey(s => s.OutdoorActivityId);
            modelBuilder.Entity<OutdoorActivity>(entity =>
            {
                entity.Property(e => e.Activity)
                    .HasColumnName("Activity");

                entity.Property(e => e.ActivityInfo)
                     .HasColumnName("ActivityInfo");

                entity.Property(e => e.Remarks)
                   .HasColumnName("Remarks");

                entity.Property(e => e.RecStatus)
                     .HasColumnName("RecStatus");

            });
            #endregion

            #region ExceptionalCondition table fields

            modelBuilder.Entity<ExceptionalCondition>().HasKey(s => s.ExceptionalConditionId);
            modelBuilder.Entity<ExceptionalCondition>(entity =>
            {
                entity.Property(e => e.Condition)
                    .HasColumnName("Condition");

                entity.Property(e => e.ConditionInfo)
                     .HasColumnName("ConditionInfo");

                entity.Property(e => e.RecStatus)
                     .HasColumnName("RecStatus");
            });
            #endregion

            #region Nurse table fields

            modelBuilder.Entity<Nurse>().HasKey(s => s.NurseId);
            modelBuilder.Entity<Nurse>(entity =>
            {
                entity.Property(e => e.NurseGuid)
                    .HasColumnName("NurseGuid");

                entity.Property(e => e.FirstName)
                     .HasColumnName("FirstName");

                entity.Property(e => e.LastName)
                 .HasColumnName("LastName");

                entity.Property(e => e.Gender)
                    .HasColumnName("Gender");

                entity.Property(e => e.Email)
                   .HasColumnName("Email");

                entity.Property(e => e.MobileNumber)
                    .HasColumnName("MobileNumber");

                entity.Property(e => e.Remarks)
                     .HasColumnName("Remarks");

                entity.Property(e => e.RecStatus)
                     .HasColumnName("RecStatus");
            });

            #endregion

            #region Child table fields

            modelBuilder.Entity<Child>().HasKey(s => s.ChildId);
            modelBuilder.Entity<Child>(entity =>
            {
                entity.Property(e => e.ChildGuid)
                    .HasColumnName("ChildGuid");

                entity.Property(e => e.FirstName)
                     .HasColumnName("FirstName");

                entity.Property(e => e.LastName)
                 .HasColumnName("LastName");

                entity.Property(e => e.ChildsGender)
                    .HasColumnName("ChildsGender");

                entity.Property(e => e.Age)
                   .HasColumnName("Age");

                entity.Property(e => e.Remarks)
                     .HasColumnName("Remarks");

                entity.Property(e => e.RecStatus)
                     .HasColumnName("RecStatus");

            });

            #endregion

            #region Breakfast table fields

            modelBuilder.Entity<Breakfast>().HasKey(s => s.BreakfastId);
            modelBuilder.Entity<Breakfast>(entity =>
            {
                entity.Property(e => e.Remarks)
                    .HasColumnName("Remarks");

                entity.Property(e => e.Amounts)
                     .HasColumnName("Amounts")
                     .HasConversion<int>();

                entity.Property(e => e.RecStatus)
                     .HasColumnName("RecStatus");

            });

            #endregion

            #region Lunch table fields

            modelBuilder.Entity<Lunch>().HasKey(s => s.LunchId);
            modelBuilder.Entity<Lunch>(entity =>
            {
                entity.Property(e => e.Remarks)
                    .HasColumnName("Remarks");

                entity.Property(e => e.Amounts)
                     .HasColumnName("Amounts")
                     .HasConversion<int>();

                entity.Property(e => e.RecStatus)
                     .HasColumnName("RecStatus");

            });

            #endregion

            #region Brunch table fields

            modelBuilder.Entity<Brunch>().HasKey(s => s.BrunchId);
            modelBuilder.Entity<Brunch>(entity =>
            {
                entity.Property(e => e.Remarks)
                    .HasColumnName("Remarks");

                entity.Property(e => e.Amounts)
                     .HasColumnName("Amounts")
                     .HasConversion<int>();

                entity.Property(e => e.RecStatus)
                     .HasColumnName("RecStatus");
            });

            #endregion

            #region ActivityAndException table fields

            modelBuilder.Entity<ActivityAndException>().HasKey(s => s.ActivityAndExceptionId);
            modelBuilder.Entity<ActivityAndException>(entity =>
            {
                entity.Property(e => e.Remarks)
                    .HasColumnName("Remarks");

                entity.Property(e => e.IncidentsTime)
                     .HasColumnName("IncidentsTime");

                entity.Property(e => e.RecStatus)
                     .HasColumnName("RecStatus");
            });

            #endregion

            #region BaseEntity table fields

            modelBuilder.Entity<ActivityAndException>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnName("CreatedDate");

                entity.Property(e => e.ModifiedDate)
                     .HasColumnName("ModifiedDate");

                entity.Property(e => e.CreatedBy)
                     .HasColumnName("CreatedBy");

                entity.Property(e => e.ModifiedBy)
                  .HasColumnName("ModifiedBy");
            });

            #endregion

            #endregion
        }
    }
}
